const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': isProd ? 'error' : 'off',
    'no-debugger': isProd ? 'error' : 'off',
    'space-before-function-paren': 'off',
    'prefer-const': 'off',
    'object-curly-spacing': 'off',
    'no-prototype-builtins': 'off',
    'vue/no-unused-vars': isProd ? 'error' : 'warn',
    'vue/no-unused-components': isProd ? 'error' : 'warn'
  },
  globals: {
    Base64: true,
    lodash: true
  }
}
