# xlsxio

xlsx 格式数据导入/导出支持包。

## 安装

```shell
npm install @wangankeji/xlsxio
```

### 依赖

- [ExcelJS](https://github.com/exceljs/exceljs)
- [JSZip](https://stuk.github.io/jszip/)

## 用法

```js
import {WorkbookDef, SheetDef, ColumnDef, ColumnEnum} from 'xlsxio'

const wb = new WorkbookDef([
  new SheetDef('Sheet1', [
    new ColumnDef('列名称', '字段名称', {
      help: '此列的帮助信息',
      headerStyle: {
        width: 20,
        size: 12
      }
    }),
  ], {
    help: '此表的帮助信息'
  }),
  new SheetDef('Sheet2', [
    new ColumnDef('列名称', '字段名称', {
      help: '此列的帮助信息'
    }),
  ])
])

// 从文件导入数据, file 是通过 <input type="file" /> 选择的文件
const data = wb.read(file)
// 将数据写入文件，可以通过第二个参数指定文件名，指定时会下载文件
const blobData = wb.write([{
  name: 'Sheet1',
  rows: []
}, {
  name: 'Sheet2',
  rows: []
}])
// 下载导入模板文件
// true 表示需要输出帮助信息
wb.write([], '模板文件名', true)
```

`rows` 是表格的数据集合。每行的数据可以是与 ColumnDef 对应的 `object`，也可以是一个简单 `array`。

> 详细用法见 [src/App.vue](./src/App.vue)

## API

- `WorkbookDef` 工作薄定义对象，这是操作 Excel 的入口
- `SheetDef` 表定义对象，一个工作薄中可以包含多个表
- `ColumnDef` 列定义对象，一个表中可以包含多个列

## 后记

为了让单元格注释能自动适应内容大小，对原始的输出进行了处理:

文件 _xl/drawings/vmlDrawing2.vml_

```xml
<v:textbox><div style="text-align:left;"></div></v:textbox>
// 替换为
<v:textbox style="mso-fit-shape-to-text:t"><div style="text-align:left;"></div></v:textbox>
```

需要关注 [PR #1933](https://github.com/exceljs/exceljs/pull/1933)，若其被合并，
则需要移除此操作，并在输出注释参数上添加 `autoShape: true` 
