const externals = process.env.NODE_ENV === 'development' ? {} : {
  exceljs: 'exceljs',
  jszip: 'jszip'
}

module.exports = {
  productionSourceMap: false,

  chainWebpack: config => {
    // 配置 Babel 转换器
    config.module
      .rule('js')
      .test(/\.m?js$/)
      .exclude
      .add(/node_modules/)
      .end()
      .use('babel-loader')
      .loader('babel-loader')
      .options({
        presets: [
          [
            '@babel/preset-env',
            {
              targets: {
                ie: '11',
                edge: '18',
                firefox: '52',
                chrome: '45',
                safari: '11.1',
              },
              useBuiltIns: 'usage',
              corejs: '3',
              modules: false,
            },
          ],
        ],
      })
      .end();

  //   // 输出配置不变
  //   config.output
  //     .library('xlsxio')
  //     .libraryExport('default');
  },
  configureWebpack: {
    externals
  }
};
